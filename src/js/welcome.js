'use strict';

export default (message) => {
    alert(`Welcome to the ${message}`);
    if (NODE_ENV === 'development') {
        console.log(message);
    }
};
